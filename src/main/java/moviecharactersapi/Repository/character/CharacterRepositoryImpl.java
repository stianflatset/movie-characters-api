package moviecharactersapi.Repository.character;

import moviecharactersapi.Repository.character.CharacterRepository;
import moviecharactersapi.logging.LogToConsole;
import moviecharactersapi.models.Character;
import moviecharactersapi.models.Franchise;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;

@Repository
public class CharacterRepositoryImpl implements CharacterRepository {

    private final LogToConsole logger;

    public CharacterRepositoryImpl(LogToConsole logger) {
        this.logger = logger;
    }

    @Autowired
    private JdbcTemplate jdbcTemplate;

    //Introduction to Character page
    public String welcomeToCharacters() {
        String header = "----------- CHARACTER PAGE -----------";
        String intro = "</br>Add the following to your URL to access commands!";
        String getCharacters = "</br>/all - See all the characters in the DB";
        String getMovieCharacters = "</br>/movie/{movieID} - Add the movie ID to the URL to see all the characters in it";
        String getFranchiseCharacters="</br>/franchise/{franchiseId} - Add the Franchise ID to the URL to see all the characters in it";
        String deleteCharacters = "</br>/delete/{characterID} - Delete a character by typing in their ID";
        String updateCharacter = "</br>/update/{characterId}/{fullName}/{alias}/{gender}/{picture}/{movieId} - Type in the ID of the user you want to update and fill in the data";
        String createCharacter = "</br>/create/{fullName}/{alias}/{gender}/{picture}/{movieId} - Add a character to the database using this command";
        return header + intro + getCharacters + getMovieCharacters + getFranchiseCharacters + deleteCharacters + updateCharacter + createCharacter;
    }

    //Getting all characters in a movie
    public ArrayList<Character> getAllMovieCharacters(int movieId) {
        ArrayList<Character> characters = new ArrayList<>();
        //Make SQL query
        jdbcTemplate.query(
                "SELECT * FROM Character WHERE movie_id = ?",
                ps -> ps.setInt(1, movieId),
                (ResultSetExtractor<String>) resultSet -> {
                    while (resultSet.next()) {
                        characters.add(
                                new Character(
                                        resultSet.getLong("id"),
                                        resultSet.getString("full_Name"),
                                        resultSet.getString("alias"),
                                        resultSet.getString("gender"),
                                        resultSet.getString("picture"),
                                        resultSet.getInt("movie_Id"),
                                        resultSet.getInt("franchiseid")
                                )
                        );
                    }
                    return null;
                });
        return characters;
    }

    //Get all characters
    public ArrayList<Character> getAllCharacters() {
        ArrayList<Character> characters = new ArrayList<>();
        //Make SQL query
        jdbcTemplate.query(
                "SELECT * FROM Character",
                (ResultSetExtractor<String>) resultSet -> {
                    while (resultSet.next()) {
                        characters.add(
                                new Character(
                                        resultSet.getLong("id"),
                                        resultSet.getString("full_Name"),
                                        resultSet.getString("alias"),
                                        resultSet.getString("gender"),
                                        resultSet.getString("picture"),
                                        resultSet.getInt("movie_Id"),
                                        resultSet.getInt("franchiseid")
                                )
                        );
                    }
                    return null;
                });
        return characters;
    }

    //Getting all characters in a franchise
    public ArrayList<Character> getAllFranchiseCharacters(int franchiseId) {
        ArrayList<Character> characters = new ArrayList<>();
        //Make SQL query
        jdbcTemplate.query(
                "SELECT * FROM Character WHERE franchise_id = ?",
                ps -> ps.setInt(1, franchiseId),
                (ResultSetExtractor<String>) resultSet -> {
                    while (resultSet.next()) {
                        characters.add(
                                new Character(
                                        resultSet.getLong("id"),
                                        resultSet.getString("full_Name"),
                                        resultSet.getString("alias"),
                                        resultSet.getString("gender"),
                                        resultSet.getString("picture"),
                                        resultSet.getInt("movie_Id"),
                                        resultSet.getInt("franchiseid")
                                )
                        );
                    }
                    return null;
                });
        return characters;
    }

    public String deleteCharacter(int characterId) {
        jdbcTemplate.update(
                "DELETE FROM Character WHERE id = ?",
                ps -> ps.setLong(1,characterId)
        );
        return "Successfully deleted character";
    }

    public String updateCharacter(int characterId, String fullName, String alias, String gender, String picture, int movieId) {
        String sql = "UPDATE Character SET full_name=?, alias=?, gender=?, picture=?, movie_id=? WHERE id=?";
        jdbcTemplate.update(sql, fullName, alias, gender, picture, movieId, characterId);
        return "Character successfully updated";
    }

    public String createCharacter(String fullName, String alias, String gender, String picture, int movieId) {
        String sql = "INSERT INTO Character(full_name, alias, gender, picture, movie_id VALUES(?,?,?,?,?)";
        jdbcTemplate.update(sql, fullName, alias, gender, picture, movieId);
        return "Character successfully created";
    }

    public void saveCharacter(Character character) {
        String sql = "INSERT INTO Character (id, full_name, alias, gender, picture, movie_id, franchiseid) VALUES (?,?,?,?,?,?,?) ON CONFLICT DO NOTHING";
        jdbcTemplate.update(sql, character.getId(), character.getFullName(), character.getAlias(), character.getGender(), character.getPicture(), character.getMovieId(), character.getFranchiseId());
    }
}