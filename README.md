
# Java Web API and Postgres database with Spring

## Group members:
* Stian Tonning Flatset
* Sigve Eilertsen


## To run application:
* Postgres installed and database "postgres". 
* Remember to go into application.properties and change username & password
* Application will run on localhost:8080


# API requests:
* To see Open API go to localhost:8080/api
* To see Swagger go to localhost:8080/api.html


# Characters (localhost:8080/api/characters)
* /all -> See all characters in the database
* /movie/{movieID} - Add the movie ID to the URL to see all the characters in it
*  /franchise/{franchiseId} - Add the Franchise ID to the URL to see all the characters in it
* /delete/{characterID} - Delete a character by typing in their ID
* /update/{characterId}/{fullName}/{alias}/{gender}/{picture}/{movieId} - Type in the ID of the user you want to update and fill in the data
* /create/{fullName}/{alias}/{gender}/{picture}/{movieId} - Add a character to the database using this command

# Movies (localhost:8080/api/movie)
* /all - See all the movies in the DB
* /{franchiseId} - Add the franchise ID to the URL to see all the movies in it
* /delete/movieID - Delete a movie by typing in its ID
* /update/{characterId}/{fullName}/{alias}/{gender}/{picture}/{movieId} - Type in the ID of the user you want to update and fill in the data
* /create/{fullName}/{alias}/{gender}/{picture}/{movieId} - Add a character to the database using this command

# Franchises (localhost:8080/api/franchise)
* /all - See all franchises
* /delete/{franchiseId} - Delete a franchise by typing in its ID
* /update/{franchiseId}/{name}/{description} - Type in the ID of the franchise you want to update and fill in the data
* /create/{name}/{description} - Add a franchise to the database using this command
