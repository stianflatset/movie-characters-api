package moviecharactersapi.models;

import org.springframework.beans.factory.annotation.Autowired;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name="character")
public class Character implements Serializable {

   /* @OneToMany(cascade = CascadeType.ALL, mappedBy = "character")
    private List<CharactersInMovies> charactersInMoviesCollection;*/

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 50, nullable = false, name="full_name")
    private String fullName;

    @Column(length = 50, name="alias")
    private String alias;

    @Column(length = 25, name="gender")
    private String gender;

    @Column(name="picture")
    private String picture;

    @Column(name="movieId")
    private int movieId;

    @Column(name="franchiseID")
    private int franchiseId;


    @Autowired
    public Character(long id, String fullName, String alias, String gender, String picture, int movieId, int franchiseId) {
        this.id = id;
        this.fullName = fullName;
        this.alias = alias;
        this.gender = gender;
        this.picture = picture;
        this.movieId = movieId;
        this.franchiseId = franchiseId;
    }

    //Creating getters and setters
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public int getMovieId() {
        return movieId;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }

    public int getFranchiseId() {return franchiseId;}

    public void setFranchiseId(int franchiseId) {this.franchiseId = franchiseId;}

    // @OneToMany
   // @JoinColumn(name="movie_id")
  //  List<Character> characters;

}
